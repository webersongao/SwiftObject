//
//  BaseRouter.swift
//  MySwiftObject
//
//  Created by anscen on 2022/4/14.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
import MGJRouter_Swift
import SwiftyJSON
import HandyJSON
public let AppShelf    = "app://shelf"
public let AppShop     = "app://shop"
public let AppClassify = "app://classify"
public let AppMy       = "app://my"
public let AppDetail   = "app://detail"
public let AppRead     = "app://read"
struct BaseRouter {
    public static func startRouter(){
        MGJRouter.registerWithObjectHandler(AppShelf) { routerParameters in
            return GKBookShelfController()
        }
        MGJRouter.registerWithObjectHandler(AppShop) { routerParameters in
            return GKShopController()
        }
        MGJRouter.registerWithObjectHandler(AppClassify) { routerParameters in
            return GKClassifyTabController()
        }
        MGJRouter.registerWithObjectHandler(AppMy) { routerParameters in
            return GKMyController()
        }
        MGJRouter.registerWithHandler(AppDetail) { routerParameters in
            let json = JSON(routerParameters as Any)
            let router = GKRouter.deserialize(from: json.dictionary)
            if let router = router {
                let bookId = router.info?["bookId"].rawString()
                let vc  = GKDetailViewController.vcWithBookId(bookId: bookId ?? "")
                vc.hidesBottomBarWhenPushed = true;
                UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true);
            }
        }
        MGJRouter.registerWithHandler(AppRead) { routerParameters in
            let json = JSON(routerParameters as Any)
            let router = GKRouter.deserialize(from: json.dictionary)
            if let router = router {
                let bookId = router.info?["bookId"].stringValue ?? ""
                let chapter = router.info?["chapter"].intValue ?? 0
                let vc = GKNovelTabController(bookId: bookId, chapter: chapter)
                vc.hidesBottomBarWhenPushed = true;
                UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
class GKRouter :HandyJSON{
    var info :JSON?   = nil
    var url  :String? = nil
    func mapping(mapper: HelpingMapper) {
        mapper <<< self.info <-- ["MGJRouterParameterUserInfo","info"]
        mapper <<< self.url <-- ["MGJRouterParameterURL","url"]
    }
    required public init() {}
}
