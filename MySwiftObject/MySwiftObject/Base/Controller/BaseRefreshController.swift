//
//  BaseRefreshController.swift
//  GKGame_Swift
//
//  Created by wangws1990 on 2018/9/30.
//  Copyright © 2018 wangws1990. All rights reserved.
//
import UIKit

@_exported import ATRefresh_Swift
@_exported import AudioToolbox

public let RefreshPageStart : Int = (1)
public let RefreshPageSize  : Int = (20)

class BaseRefreshController: BaseViewController {
    public lazy var refreshData: ATRefreshData = {
        let refresh = ATRefreshData()
        refresh.delegate = self
        refresh.dataSource = self
        return refresh
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    public func setupRefresh(scrollView :UIScrollView,options :ATRefreshOption){
        self.refreshData.setupRefresh(scrollView: scrollView, options: options)
    }
    public func endRefresh(more :Bool,empty :String = "数据空空如也...",image :String = "icon_data_empty"){
        self.refreshEmptyToast = empty
        self.refreshEmptyData = image
        self.refreshData.endRefresh(more: more)
    }
    public func endRefreshFailure(error :String = "网络出现异常...",image :String = "icon_data_error"){
        self.refreshEmptyToast = error
        self.refreshEmptyData = image
        self.refreshData.endRefreshFailure()
    }
    private let refreshLoaderToast :String  = "数据加载中..."
    private var refreshEmptyToast  :String  = ""
    private var refreshEmptyData   :String  = ""
    private lazy var images: [UIImage] = {
        var images :[UIImage] = [];
        for i in 0...4{
            let image = UIImage(named:"loading\(i)")
            if let im = image{
                images.append(im);
            }
        }
        for i in 0...4{
            let image = UIImage(named:"loading\(4-i)")
            if let im = image{
                images.append(im);
            }
        }
        return images;
    }()
    private lazy var imageing: UIImage = {
        let image  = UIImage(named: "icon_data_loading")
        return image ?? UIImage()
    }()
}
extension BaseRefreshController : ATRefreshDataSource{
    var refreshHeaderData: [UIImage] {
        return self.images
    }
    var refreshFooterData: [UIImage] {
        return self.images
    }
    var refreshLogo: UIImage{
        if self.refreshData.refreshing {
            return self.imageing.imageColor()
        }
        return UIImage(named:self.refreshEmptyData) ?? UIImage()
    }
    var refreshAnimation: CAAnimation{
        let animation = CABasicAnimation(keyPath: "transform")
        animation.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        animation.toValue = NSValue(caTransform3D: CATransform3DMakeRotation(CGFloat(Double.pi / 2), 0, 0, 1.0))
        animation.duration = 0.3
        animation.isCumulative = true
        animation.repeatCount = MAXFLOAT
        return animation
    }
    var refreshTitle: NSAttributedString{
        let color = self.refreshData.refreshing ? moya.appColor : UIColor.appx999999()
        let subTitle = self.refreshData.refreshing ? self.refreshLoaderToast : self.refreshEmptyToast
        return NSAttributedString(string:subTitle, attributes:[.font :UIFont.systemFont(ofSize: 15),.foregroundColor :color])
    }
    var refreshVertica: CGFloat{
        return -(NAVI_BAR_HIGHT)/2
    }
    var refreshColor: UIColor{
        return UIColor.appxffffff()
    }
}
extension BaseRefreshController : ATRefreshDelegate{
    @objc public func refreshData(page:Int){}
}
