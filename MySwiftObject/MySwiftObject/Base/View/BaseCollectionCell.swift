//
//  BaseCollectionCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/14.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
class BaseCollectionCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadBaseUI()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        //self.loadBaseUI()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadBaseUI()
        // Initialization code
    }
    func loadBaseUI(){
        self.backgroundColor = UIColor.appxffffff()
        self.contentView.backgroundColor = UIColor.appxffffff()
    }
}
