//
//  BaseTableCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/14.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class BaseTableCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.loadBaseUI()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        //self.loadBaseUI()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadBaseUI()
    }
    func loadBaseUI(){
        self.backgroundColor = UIColor.appxffffff()
        self.contentView.backgroundColor = UIColor.appxffffff()
    }
}
