//
//  BaseView.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/14.
//  Copyright © 2022 wangws1990. All rights reserved.
//
import UIKit

class BaseView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadBaseUI()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        //self.loadBaseUI()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadBaseUI()
    }
    func loadBaseUI(){
        self.backgroundColor = UIColor.appxffffff()
    }
}
