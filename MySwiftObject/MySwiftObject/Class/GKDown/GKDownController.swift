//
//  GKDownController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/4/15.
//  Copyright © 2022 wangws1990. All rights reserved.
//
import UIKit

class GKDownController: BaseTableViewController {
    
    lazy var listData: [GKContent] = {
        return []
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "我的下载")
        self.setupRefresh(scrollView: self.tableView, options: .defaults)
    }
    override func refreshData(page: Int) {
//        DispatchQueue.global().async {
//            let list = GKRegular.txtBook()
//            DispatchQueue.main.async {
//                //self.listData = list
//                self.tableView.reloadData()
//                self.endRefresh(more: false)
//            }
//        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = GKNovelDirectoryCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        cell.titleLab.text = self.listData[indexPath.row].title
        cell.imageLock.isHidden = true
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.listData[indexPath.row]
        let vc = GKDownItemController(model: item)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

