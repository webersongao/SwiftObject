//
//  GKManTabView.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
protocol GKManTabDelegate :NSObjectProtocol {
    func manTabView(tabView :GKManTabView,type :GKManTabType)
}
class GKManTabView: UIView {
    public weak var delegate : GKManTabDelegate? = nil
    @IBOutlet weak var collectionView: UICollectionView!
    lazy var listData: [GKManTabType] = {
        return [.right,.britess,.mulu]
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
    }
}
extension GKManTabView :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0);
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Int(collectionView.frame.size.width)/self.listData.count
        return CGSize(width: CGFloat(width - 1), height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell   = GKNovelTabCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
        let model  = self.listData[indexPath.row];
        cell.imageV.image = UIImage(named:model.rawValue)
        cell.width.constant = 49
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let delegate = self.delegate else { return }
        delegate.manTabView(tabView: self, type: self.listData[indexPath.row])
    }
}
