//
//  GKManCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKManCell: UITableViewCell {
    @IBOutlet weak var imageV: GKAnimatedImageView!
    var model : GKManItem?{
        didSet{
            guard let item = model else { return }
            self.imageV.setGIFImageWithURL(url: item.location)
        }
    }
    
}
