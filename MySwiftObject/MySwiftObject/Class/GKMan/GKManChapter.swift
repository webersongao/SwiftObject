//
//  GKManChapter.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
import HandyJSON

class GKManChapter: HandyJSON {
    var chapterId  :String = ""
    var name       :String = ""
    var new        :Bool   = false
    var type       :Int    = 0
    var total      :Int    = 0
    required init() {}
    func mapping(mapper: HelpingMapper) {
        mapper <<< self.chapterId <-- ["chapter_id","chapterId"]
        mapper <<< self.new <-- ["new","is_new"]
        mapper <<< self.total <-- ["image_total","total"]
    }
}
