//
//  GKBookCaseGridCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/4/7.
//  Copyright © 2022 wangws1990. All rights reserved.
//
import UIKit

class GKBookCaseGridCell: BaseCollectionCell {
    @IBOutlet weak var top: UIButton!
    @IBOutlet weak var newBtn: UIButton!
    
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageV: UIImageView!

    public static var inset : UIEdgeInsets{
        return UIEdgeInsets(top: moya.listView ? 0 : 20 , left: moya.listView ? 0 : 10, bottom: moya.listView ? 0 : 10, right: moya.listView ? 0 : 10)
    }
    public static var cellSize : CGSize{
        let width = (SCREEN_WIDTH - 40)/3 - 0.1
        return CGSize(width:width, height: (width - 10) * 1.4 + 10 + 42)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageV.layer.masksToBounds = true
        self.imageV.layer.cornerRadius = 3
        self.newBtn.isHidden = true
        self.titleLab.textColor = UIColor.appx333333()
    }
    var book : GKBook?{
        didSet{
            guard let item = book else { return  }
            self.imageV.setGkImageWithURL(url: item.cover ?? "")
            self.titleLab.text = item.title ?? ""
           // self.newBtn.isHidden = item.new ? false : true
            self.top.isHidden = item.topTime > 0 ? false : true
            //self.cateBtn.setTitle(item.majorCate ?? "", for: .normal)
        }
    }
}
