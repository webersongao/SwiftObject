//
//  GKActionSheetCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/16.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKActionSheetCell: BaseCollectionCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLab: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var model :GKActionSheet?{
        didSet{
            guard let item = model else { return }
            self.imageV.image = UIImage(named: item.icon)
            self.titleLab.text = item.title.rawValue
        }
    }
}
