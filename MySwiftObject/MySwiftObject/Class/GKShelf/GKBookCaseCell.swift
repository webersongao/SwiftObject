//
//  GKBookCaseCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/11/6.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit

protocol GKBookCaseCellDelegate :NSObjectProtocol {
    func moreTapAction(model :GKBook?)
}

class GKBookCaseCell: BaseCollectionCell {
    @IBOutlet weak var topBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var subTitleLab: UILabel!
    
    @IBOutlet weak var cateBtn: UIButton!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var contentLab: UILabel!

    public weak var delegate :GKBookCaseCellDelegate? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLab.textColor = UIColor.appx333333()
        self.subTitleLab.textColor = UIColor.appx666666()
        self.contentLab.textColor = UIColor.appx999999()
        self.lineView.backgroundColor = UIColor.appxdddddd()
        self.topBtn.layer.masksToBounds = true
        self.topBtn.layer.cornerRadius = 2
        
        self.cateBtn.layer.masksToBounds =  true
        self.cateBtn.layer.cornerRadius = 5
        self.cateBtn.layer.borderWidth = 0.8
        self.cateBtn.layer.borderColor = UIColor(hex: "dddddd").cgColor
        self.imageV.layer.masksToBounds = true
        self.imageV.layer.cornerRadius = AppRadius
        self.updateBtn.isHidden = true
    }
    @IBAction func moreAction(_ sender: UIButton) {
        guard let delegate = self.delegate else { return }
        delegate.moreTapAction(model: self.book)
    }
    public static var cellSize : CGSize{
        let width = (SCREEN_WIDTH - 10)/3 - 0.1
        return CGSize(width:width, height: (width - 10) * 1.35 + 10 + 45)
    }
    var book : GKBook?{
        didSet{
            guard let item = book else { return  }
            self.imageV.setGkImageWithURL(url: item.cover ?? "")
            self.titleLab.text = item.title ?? ""
            self.subTitleLab.text = item.author
            self.contentLab.attributedText = attTitle(content: item.shortIntro ?? "")
           // self.updateBtn.isHidden = item.new ? false : true
            self.topBtn.isHidden = item.topTime > 0 ? false : true
            self.cateBtn.setTitle(item.majorCate ?? "", for: .normal)
        }
    }
    func attTitle(content :String) -> NSAttributedString {
        let paragraphStyle  = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = .left
        paragraphStyle.allowsDefaultTighteningForTruncation = true
        let att = NSAttributedString(string:content.count > 0 ? content : "这家伙很懒,暂无简介!", attributes:[NSAttributedString.Key.paragraphStyle : paragraphStyle,NSAttributedString.Key.foregroundColor : UIColor.appx999999()])
        return att
    }
    
}
