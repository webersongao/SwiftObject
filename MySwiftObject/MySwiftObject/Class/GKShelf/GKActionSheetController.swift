//
//  GKActionSheetController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/16.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
protocol GKActionSheetDelegate :NSObjectProtocol {
    func actionSheet(model :GKActionSheet,book :GKBook)
}
class GKActionSheetController: BaseConnectionController {

    convenience init(delegate :GKActionSheetDelegate,book :GKBook) {
        self.init()
        self.delegate = delegate
        self.book = book
    }
    private weak var delegate :GKActionSheetDelegate? = nil
    private var book :GKBook? = nil
    private lazy var listData: [GKActionSheet] = {
        return []
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRefresh(scrollView: self.collectionView, options: .auto)
    }
    override func refreshData(page: Int) {
        guard let model = book else {
            return
        }
        let item1 = GKActionSheet(title: model.topTime > 0 ? .bot : .top, icon: "ic_book_case_top");
        let item2 = GKActionSheet(title: .mulu, icon: "ic_book_case_mulu");
        let item3 = GKActionSheet(title: .deleBook, icon: "ic_book_case_delete");
        let item4 = GKActionSheet(title: .down, icon: "ic_book_case_down");
        let item5 = GKActionSheet(title: .move, icon: "ic_book_case_move");
        self.listData = [item1,item2,item3,item4,item5]
        self.collectionView.reloadData()
        self.endRefresh(more: false)
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREEN_WIDTH/5 - 0.1, height: 100)
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.listData[indexPath.row]
        let cell = GKActionSheetCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
        cell.model = item
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let delegate = self.delegate else { return }
        guard let book = self.book else { return }
        self.goBack()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            let item = self.listData[indexPath.row]
            delegate.actionSheet(model: item,book:book)
        }
    }
    //底部弹框这个要设置
    override func showDragIndicator() -> Bool {
        return false
    }
    override func longFormHeight() -> PanModalHeight {
        return PanModalHeightMake(.content, CGFloat(150))
    }
    override func dismissalDuration() -> TimeInterval {
        return 0.3
    }
    
    override func springDamping() -> CGFloat {
        return 1
    }
    override func cornerRadius() -> CGFloat {
        return 15
    }
    override func backgroundConfig() -> HWBackgroundConfig {
        let config = HWBackgroundConfig()
        config.backgroundAlpha = 0.3
        return config
    }
    override func shouldRespond(toPanModalGestureRecognizer panGestureRecognizer: UIPanGestureRecognizer) -> Bool {
        return false
    }
}
