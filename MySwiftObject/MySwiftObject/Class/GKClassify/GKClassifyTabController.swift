//
//  GKClassifyTabController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKClassifyTabController: BaseViewController,VTMagicViewDelegate,VTMagicViewDataSource {
//MARK: 文件初始化
    private lazy var listData:[String] = {
        return ["male","female","press","picture"]
    }()
    private lazy var listTitles:[String] = {
        return ["男生","女生","出版社","其他"]
    }()
    lazy var controllers: [BaseViewController] = {
        return []
    }()
    private lazy var magicViewCtrl: VTMagicController = {
        let ctrl = VTMagicController()
        
        ctrl.magicView.navigationInset = UIEdgeInsets(top: STATUS_BAR_HIGHT, left: 15, bottom: 0, right: 0)
        ctrl.magicView.switchStyle = .default;
        
        ctrl.magicView.layoutStyle = .default
        ctrl.magicView.itemSpacing = 30
        ctrl.magicView.navigationHeight = NAVI_BAR_HIGHT
        ctrl.magicView.sliderHeight = 2
        ctrl.magicView.sliderOffset = 0
        ctrl.magicView.isAgainstStatusBar = false;
        ctrl.magicView.dataSource = self;
        ctrl.magicView.delegate = self;
        ctrl.magicView.itemScale = 1;
        ctrl.magicView.needPreloading = true;
        ctrl.magicView.bounces = false;
        ctrl.magicView.isScrollEnabled = true
        ctrl.magicView.sliderStyle = .bubble
        ctrl.magicView.bubbleInset = UIEdgeInsets(top: 4, left: 13, bottom: 4, right: 13)
        ctrl.magicView.bubbleRadius = 13
        return ctrl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUI();
    }
    private func loadUI() {
        self.fd_prefersNavigationBarHidden = true;
        self.magicViewCtrl.magicView.backgroundColor = UIColor.appxffffff()
        self.magicViewCtrl.magicView.separatorColor = UIColor.appxffffff()
        self.magicViewCtrl.magicView.navigationColor = UIColor.appxffffff()
        self.magicViewCtrl.magicView.backgroundColor = UIColor.appxffffff()
        self.magicViewCtrl.magicView.sliderColor = UIColor.appxf1f2f4()
        self.addChild(self.magicViewCtrl);
        self.view.addSubview(self.magicViewCtrl.view);
        self.magicViewCtrl.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.listData.forEach { titleName in
            let vc =  GKClassifyItemController(titleName: titleName)
            self.controllers.append(vc)
        }
        self.magicViewCtrl.magicView.reloadData()
    }
    func menuTitles(for magicView: VTMagicView) -> [String] {
        return self.listTitles;
    }
    func magicView(_ magicView: VTMagicView,menuItemAt: UInt) -> UIButton {
        let button = magicView.dequeueReusableItem(withIdentifier: "com.new.btn.itemIdentifier") ?? UIButton(type: .custom)
        button.setTitleColor(UIColor.appx333333(), for: .normal)
        button.setTitleColor(moya.appColor, for: .selected)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        return button
    }
    func magicView(_ magicView: VTMagicView, viewControllerAtPage pageIndex: UInt) -> UIViewController {
        return self.controllers[Int(pageIndex)]
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return moya.night ? .lightContent : .default;
    }
}
