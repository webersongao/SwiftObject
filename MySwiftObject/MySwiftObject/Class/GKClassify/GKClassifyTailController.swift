//
//  GKClassifyTailController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//
import UIKit

class GKClassifyTailController: BaseTableViewController {
    convenience init(group:String? = nil,name:String? = nil,rankId :String? = nil) {
        self.init();
        self.group = group
        self.name = name
        self.rankId = rankId
    }
    private var group  :String? = nil
    private var name   :String? = nil
    private var rankId :String? = nil
    private lazy var listData: [GKBook] = {
        return []
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: self.name)
        self.setupRefresh(scrollView: self.tableView, options:.defaults);
    }
    override func refreshData(page: Int) {
        guard let rankId = self.rankId else {
            loadClassifyData(page: page)
            return
        }
        loadMoreData(page: page,rankId: rankId)
    }
    private func loadClassifyData(page: Int){
        guard let group = self.group else { return self.endRefresh(more: false) }
        guard let name = self.name else { return self.endRefresh(more: false) }
        ApiMoya.request(target: .classifyTail(group, name: name, page: page)) { json in
            if page == RefreshPageStart{
                self.listData.removeAll();
            }
            guard let list = [GKBook].deserialize(from: json["books"].rawString()) else { return self.endRefresh(more: false) }
            if let data = list as? [GKBook]{
                self.listData.append(contentsOf: data)
            }
            self.tableView.reloadData();
            self.endRefresh(more: list.count > 0 ? true : false)
        } failure: { error in
            self.endRefreshFailure(error: error)
        }
    } 
    private func loadMoreData(page: Int,rankId :String) {
        ApiMoya.request(target: .homeHot(rankId)) { json in
            guard let info =  GKHomeInfo.deserialize(from: json["ranking"].rawString())else {
                return  self.endRefreshFailure()
            }
            if page == RefreshPageStart{
                self.listData.removeAll()
            }
            self.listData.append(contentsOf: info.books)
            self.tableView.reloadData()
            self.endRefresh(more: false)
        } failure: { error in
            self.endRefreshFailure(error: error)
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = GKClassifyTailCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        cell.model = self.listData[indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated:true)
        let model = self.listData[indexPath.row]
        GKJump.jumpToDetail(bookId: model.bookId)
    }
}

