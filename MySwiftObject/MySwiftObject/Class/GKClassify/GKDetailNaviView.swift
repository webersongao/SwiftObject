//
//  GKDetailNaviView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/12/10.
//  Copyright © 2020 wangws1990. All rights reserved.
//
import UIKit

class GKDetailNaviView: BaseView {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var muluButton: UIButton!
    @IBOutlet weak var titleLab: UILabel!
    var model :GKBookInfo?{
        didSet{
            guard let item = model else { return }
            self.titleLab.text = item.title ?? ""
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor .clear
        self.backBtn.setImage(UIImage(named: "icon_detai_back_1"), for: .normal)
        self.backBtn.setImage(UIImage(named: "icon_detai_back_1"), for: [.normal,.highlighted])
        
        self.backBtn.setImage(UIImage(named: "icon_detai_back_2"), for: .selected)
        self.backBtn.setImage(UIImage(named: "icon_detai_back_2"), for: [.normal,.highlighted])
        
        self.muluButton.setImage(UIImage(named:"icon_read_mu")?.imageColor(UIColor.appx333333()), for: .selected)
        self.muluButton.setImage(UIImage(named: "icon_read_mu")?.imageColor(UIColor.white), for: .normal)
        
        self.setAlpha(value: 0)
        self.titleLab.textColor = UIColor.appx000000()
        self.mainView.backgroundColor = UIColor.appxffffff()
    }
    @IBAction func muluAction(_ sender: UIButton) {
        guard let item = self.model else { return }
        GKJump.jumpToMulu(bookId: item.bookId ?? "")
    }
    func setAlpha(value :CGFloat){
        self.mainView.alpha = value
        self.titleLab.isHidden = value == 1 ? false : true
        self.backBtn.isSelected = value > 0.5
        self.muluButton.isSelected = value > 0.5
    }
}
