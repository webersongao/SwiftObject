//
//  GKClassifyModel.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import HandyJSON
struct GKClassifyModel :HandyJSON {
    var title       :String? = ""
    var cover       :String? = ""
    var icon        :String? = ""
    var bookCount   :Int? = 0
    var monthlyCount:Int? = 0
    var bookCover   :[String]? = []
    mutating func mapping(mapper: HelpingMapper) {
        mapper <<< self.title     <-- ["title","name"]
    }
}

//class GKClassifyModel : Codable {
//    
//    var title       :String? = ""
//    var cover       :String? = ""
//    var icon        :String? = ""
//    var bookCount   :Int? = 0
//    var monthlyCount:Int? = 0
//    var bookCover   :[String]? = []
//    
//    enum CodingKeys: String, CodingKey {
//        case title = "name"
//        case cover
//        case monthlyCount
//        case icon
//        case bookCount
//        case bookCover
//    }
//}
