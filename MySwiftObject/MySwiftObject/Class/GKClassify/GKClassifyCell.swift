//
//  GKClassifyCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKClassifyCell: BaseCollectionCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    
    public static var cellSize : CGSize{
        let width = (SCREEN_WIDTH - 10)/3 - 0.1
        return CGSize(width:width, height: (width - 10) * 1.4 + 10 + 45)
    }
    var model : GKClassifyModel?{
        didSet{
            guard let item = model else { return  }
            self.titleLab.text = item.title ?? ""
            self.imageV.setGkImageWithURL(url: item.bookCover?.first ?? "")
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLab.textColor = UIColor.appx333333()
        self.mainView.layer.masksToBounds = true
        self.mainView.layer.cornerRadius = 5
        let layerView = UIView()
        layerView.backgroundColor = UIColor.white
        self.contentView.insertSubview(layerView, at: 0)
        layerView.snp.makeConstraints { make in
            make.edges.equalTo(self.mainView)
        }
        layerView.layer.shadowColor = UIColor(white: 0, alpha: 0.08).cgColor
        layerView.layer.shadowOffset = CGSize(width: 0, height: 0  )
        layerView.layer.shadowOpacity = 1
        layerView.layer.shadowRadius = 6
        layerView.layer.cornerRadius = 6
    }
}
