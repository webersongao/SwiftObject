//
//  GKBookBatteryView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/11/4.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit

class GKBookBatteryView: BaseView {
    lazy var timeLab : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        return label
    }()
    lazy var pageLab : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .right
        return label
    }()
    lazy var batteryView: BBatteryView = {
        return BBatteryView()
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadUI()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadUI()
    }
    private func loadUI(){
        self.addSubview(self.batteryView)
        self.addSubview(self.timeLab)
        self.addSubview(self.pageLab)
        self.batteryView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(6)
            make.width.equalTo(25)
            make.height.equalTo(10)
        }
        self.timeLab.snp.makeConstraints { make in
            make.centerY.equalTo(self.batteryView)
            make.left.equalTo(self.batteryView.snp.right).offset(10)
        }
        self.pageLab.snp.makeConstraints { make in
            make.centerY.equalTo(self.batteryView)
            make.right.equalToSuperview().offset(-20)
        }
        let color = UIColor(hex: moya.bookSet.theme.fontColor)
        self.timeLab.textColor = color
        self.pageLab.textColor = color
        self.batteryView.backgroundColor = UIColor.clear
        self.timeLab.text = Date().timeStampToDateMinute()
        self.pageLab.text = "1/1"
    }
    func reloadThemeUI(){
        let color = UIColor(hex: moya.bookSet.theme.fontColor)
        self.timeLab.textColor = color
        self.pageLab.textColor = color
        self.batteryView.setNeedsDisplay()
    }
    func loadData(pageIndex :NSInteger,total :NSInteger){
        self.timeLab.text = Date().timeStampToDateMinute()
        self.pageLab.text = "\(pageIndex)/\(total)"
        self.pageLab.isHidden = total <= 0
    }
}
class BBatteryView: BaseView {
   // public var fillColor : UIColor = UIColor(hex: moya.bookSet.theme.fontColor)
    private var batteryLevel :CGFloat{
        UIDevice.current.isBatteryMonitoringEnabled = true
        let batteryLevel = UIDevice.current.batteryLevel
        if batteryLevel <= 0 {
            return 0.01
        }else if batteryLevel >= 1 {
            return 1.0
        }
        return CGFloat(batteryLevel)
    }
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return  }
        let str  = moya.bookSet.theme.fontColor
        let fillColor = UIColor(hex: str)
        context.setLineWidth(1)
        context.setFillColor(UIColor.clear.cgColor)
        context.setStrokeColor(fillColor.cgColor)
        var newR = CGRect(x: 0.5, y: 0.5, width: rect.size.width - 4, height: rect.size.height - 1)
        context.stroke(newR)
        context.setFillColor(fillColor.cgColor)
        newR = CGRect(x: rect.size.width - 4 + 0.5, y: (rect.size.height - 4) / 2, width: 4, height: 4)
        context.fill(newR)
        let deviceLevel = self.batteryLevel - 0.01
        let width = (rect.size.width - 7)*deviceLevel
        newR = CGRect(x: 2, y: 2, width: width, height: rect.size.height-4)
        context.fill(newR)
    }
}
