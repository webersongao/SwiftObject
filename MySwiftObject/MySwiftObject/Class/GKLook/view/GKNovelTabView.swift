//
//  GKNovelTabView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/12.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
protocol GKBottomDelegate :NSObjectProtocol{
   func bottomView(bottomView :GKNovelTabView,last   :Bool)
   func bottomView(bottomView :GKNovelTabView,slider :Int)
   func bottomView(bottomView :GKNovelTabView,tabType:GKTabType)
}
class GKNovelTabView: BaseView {
    public weak var delegate :GKBottomDelegate?
    private lazy var listData: [GKTabType] = {
        return [.mulu,.day,.set]
    }()
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lastBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    override func awakeFromNib() {
        self.slider.setThumbImage(UIImage(named: "icon_slider"), for: .normal)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        let color = UIColor.appxffffff()
        self.collectionView.backgroundView?.backgroundColor = color
        self.collectionView.backgroundColor = color
        self.backgroundColor = color
        self.loadData()
    }
    func loadData(){
        self.listData = moya.bookSet.night ? [.mulu,.night,.set] : [.mulu,.day,.set]
        self.collectionView.reloadData()
    }
    @IBAction func lastAction(_ sender: UIButton) {
        guard let delegate = self.delegate else { return }
        delegate.bottomView(bottomView: self, last: true)
    }
    @IBAction func nextAction(_ sender: UIButton) {
        guard let delegate = self.delegate else { return }
        delegate.bottomView(bottomView: self, last: false)
    }
    @IBAction func sliderAction(_ sender: UISlider) {
        guard let delegate = self.delegate else { return }
        delegate.bottomView(bottomView: self, slider: Int(sender.value))
    }
}
extension GKNovelTabView :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0);
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Int(collectionView.frame.size.width)/self.listData.count
        return CGSize(width: CGFloat(width - 1), height: 35)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell   = GKNovelTabCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
        let model  = self.listData[indexPath.row];
        cell.imageV.image = UIImage(named: model.rawValue)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model  = self.listData[indexPath.row];
        if model == .night || model == .day{
            GKBookSet.setNight(model != .night)
        }
        self.loadData()
        guard let delegate = self.delegate else { return }
        delegate.bottomView(bottomView: self, tabType: model)
    }
}
