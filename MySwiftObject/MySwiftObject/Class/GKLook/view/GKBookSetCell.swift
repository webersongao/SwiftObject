//
//  GKBookSetCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/12.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKBookSetCell: BaseCollectionCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var imageV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.cornerRadius = 22.5
        self.imageV.layer.masksToBounds = true;
        self.imageV.layer.cornerRadius = 22.5;
        self.icon.isHidden = true
    }
    var model :GKBookTheme?{
        didSet{
            guard let item = model else { return }
            self.imageV.image = UIImage.imageWithColor(color: UIColor(hex: item.themeColor))
            self.icon.isHidden = moya.bookSet.theme.themeColor == item.themeColor ? false : true
        }
    }
}
