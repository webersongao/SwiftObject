//
//  GKNovelDirectoryCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/19.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import SwiftyJSON

class GKNovelDirectoryCell: BaseTableCell {
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageLock: UIImageView!
    
    @IBOutlet weak var lineView: UIView!
    var  select :Bool?{
        didSet{
            guard let select = select else { return }
            self.titleLab.textColor = select ? moya.appColor : UIColor.appx000000()
        }
    }
    var json :JSON?{
        didSet{
            guard let js = json else { return }
            self.titleLab.text = js["title"].stringValue
            self.imageLock.isHidden = !js["isVip"].boolValue
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.tag = 10086;
        self.lineView.backgroundColor = UIColor.appxe5e5e5()
        // Initialization code
    }
    
}
