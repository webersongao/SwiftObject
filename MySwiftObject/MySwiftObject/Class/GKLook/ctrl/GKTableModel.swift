//
//  GKTableModel.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/8/14.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit

struct GKTableModel {
    var chapter : NSInteger       = 0
    var content : GKContent?      = nil
}
