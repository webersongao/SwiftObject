//
//  GKLoginController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/9.
//  Copyright © 2022 wangws1990. All rights reserved.
//
import UIKit

class GKLoginController: BaseViewController {
    
    @IBOutlet weak var tipLab: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var pwd: UITextField!
    @IBOutlet weak var account: UITextField!
    var canTap :Bool?{
        didSet{
            guard let tap = canTap else { return }
            self.loginBtn.isUserInteractionEnabled = tap
            self.loginBtn.isSelected = tap
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.showNavTitle(title: "登录")
        self.pwd.addTarget(self, action: #selector(textAction(sender:)), for: .editingChanged)
        self.account.addTarget(self, action: #selector(textAction(sender:)), for: .editingChanged)
        self.loginBtn.setBackgroundImage(UIImage.imageWithColor(color: UIColor.appColor()), for: .selected)
        self.loginBtn.setBackgroundImage(UIImage.imageWithColor(color: UIColor.appColor()), for: [.selected,.highlighted])
        
        self.loginBtn.setBackgroundImage(UIImage.imageWithColor(color: UIColor.appx999999()), for: .normal)
        self.loginBtn.setBackgroundImage(UIImage.imageWithColor(color: UIColor.appx999999()), for: [.normal,.highlighted])
        self.loginBtn.layer.masksToBounds = true
        self.loginBtn.layer.cornerRadius = 5
        self.canTap = false
        self.tipLab.isHidden = true
        self.lineView()
    }
    func lineView(listData :[Int] = [100,101]){
        listData.forEach { tag in
            if let lineView = self.view.viewWithTag(tag){
                lineView.backgroundColor = UIColor.appxdddddd()
            }
        }
    }
    @IBAction func loginAction(_ sender: UIButton) {
        guard let account = self.account.text else { return }
        let user = GKUser()
        user.token = (account + "\(Date().timeStamp())").md5String
        user.account = account
        user.avatar = "/agent/http%3A%2F%2Fimg.1391.com%2Fapi%2Fv1%2Fbookcenter%2Fcover%2F1%2F288852%2F288852_d99d5338a2ce4b4b908971b24fa5ea5a.jpg%2F"
        GKLogin.loginSuccess(user)
    }
    @objc func textAction(sender :UITextField){
        self.tipLab.isHidden = true
        guard let account = self.account.text else { return }
        guard let pwd = self.pwd.text else { return }
        
        self.tipLab.isHidden = !(account.count < 6 || pwd.count < 6)
        self.canTap = account.count >= 6 && pwd.count >= 6
    }
}
