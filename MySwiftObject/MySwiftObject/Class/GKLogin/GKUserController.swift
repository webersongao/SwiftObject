//
//  GKUserController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/10.
//  Copyright © 2022 wangws1990. All rights reserved.
//
import UIKit

private let account = "账号"
private let icon    = "头像"

class GKUserController: BaseTableViewController {
    lazy var loginOutBtn: UIButton = {
        let button = UIButton(type: .custom)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0.8
        button.layer.borderColor = UIColor.appxdddddd().cgColor
        button.setTitleColor(UIColor.appx333333(), for: .normal)
        button.setTitle("登出", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.addTarget(self, action: #selector(loginOutAction), for: .touchUpInside)
        return button
    }()
    lazy var listData: [GKMyModel] = {
        return []
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "用户信息")
        self.setupRefresh(scrollView: self.tableView, options: .auto)
        let footView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 200))
        footView.addSubview(self.loginOutBtn)
        self.loginOutBtn.snp.makeConstraints { make in
            make.height.equalTo(45)
            make.bottom.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        self.tableView.tableFooterView = footView
    }
    @objc func loginOutAction(){
        ATAlertView.showAlertView(title: "确定要退出登录", message: nil, normals: ["取消"], hights: ["确定"]) { title, index in
            if index > 0 {
                GKLogin.loginOut()
                self.goBack()
            }
        }
    }
    override func refreshData(page: Int) {
        let item1 = GKMyModel(title: icon, icon: moya.user.avatar ?? "", subTitle: "", switchOn: false)
        let item2 = GKMyModel(title: account, icon: "", subTitle: moya.user.account ?? "", switchOn: false)
        self.listData = [item1,item2]
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.listData[indexPath.row]
        if item.title == icon{
            let cell = tableView.cellForTableView(tableCell: GKUserCell.self, indexPath: indexPath)
            cell.model = item
            return cell
        }
        let cell = tableView.cellForTableView(tableCell: GKNickNameCell.self, indexPath: indexPath)
        cell.model = item
        return cell
    }
}
