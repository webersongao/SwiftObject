//
//  GKLaunchController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/18.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import Lottie

class GKLaunchController: BaseViewController {
    convenience init(completion:((() -> ()))? = nil) {
        self.init()
        self.completion = completion
    }
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var skipBtn: UIButton!
    private var completion :(() -> ())? = nil
    private var timer: Timer? = nil
    lazy var lottieView : AnimationView = {
        return AnimationView(name: "N")
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUI()
    }
    @IBAction func skipAction(_ sender: UIButton) {
        self.stopTimer();
        self.dismissController();
    }
    private func loadUI(){
        self.fd_prefersNavigationBarHidden = true
        self.view.backgroundColor = UIColor.clear
        self.skipBtn.layer.masksToBounds = true
        self.skipBtn.layer.cornerRadius = 5
        self.skipBtn.layer.borderWidth = 1
        self.skipBtn.layer.borderColor = UIColor.appxf8f8f8().cgColor
        self.skipBtn.backgroundColor = UIColor.appx999999()
        self.skipBtn.titleLabel?.font = UIFont.monospacedDigitSystemFont(ofSize: 14, weight: .regular)
        self.skipBtn.setTitleColor(UIColor.white, for: .normal)
        self.view.addSubview(self.lottieView)
        self.lottieView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        self.lottieView.play { (finish) in
            
        }
        self.lottieView.loopMode = .loop
        self.progress.progressTintColor = moya.appColor
        self.progress.layer.masksToBounds = true
        self.progress.layer.cornerRadius = 2.5
        self.progress.isHidden = true
        self.startTimer()
    }
    private func startTimer(){
        stopTimer()
        var time : Int = 3
        self.setTime(time: time)
        self.timer = Timer(timeInterval: 1, repeats: true, block: { [weak self](timer) in
            print(time)
            time = time - 1
            self?.setTime(time: time)
            if time < 0{
                self?.skipAction(self!.skipBtn)
                return
            }
        });
        guard let time = self.timer else { return  }
        RunLoop.current.add(time, forMode: .common)
    }
    private func setTime(time : Int){
        self.progress.setProgress(Float((4 - time))/4.0, animated: true)
        self.skipBtn.setTitle("\(time)S跳过", for: .normal);
    }
    private func stopTimer(){
        guard let time = self.timer else { return  }
        if time.isValid{
            time.invalidate()
        }
    }
    private func dismissController(){
        guard let completion = self.completion else { return }
        completion()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    override var prefersStatusBarHidden: Bool{
        return false
    }
}
