//
//  GKGridController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/2/8.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit

class GKGridController: BaseConnectionController {
    lazy var segmentedControl: UISegmentedControl = {
        let segmentedControl = UISegmentedControl(items: ["网格模式","列表模式"])
        segmentedControl.addTarget(self, action: #selector(segementAction(sender:)), for: .valueChanged)
        return segmentedControl
    }()
    @objc func segementAction(sender : UISegmentedControl){
        ApiClient.setGrid(sender.selectedSegmentIndex == 0 ? .grid : .list)
        refreshData(page: 1)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "模式选择")
        self.setupRefresh(scrollView: self.collectionView, options: .auto)
        self.segmentedControl.selectedSegmentIndex = listView ? 1 : 0
        self.view .addSubview(self.segmentedControl)
        self.segmentedControl.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-TAB_BAR_ADDING - 30)
        }
    }
    override func refreshData(page: Int) {
        self.collectionView.reloadData()
        self.endRefresh(more: false)
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return GKBookCell.inset
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if listView {
            return CGSize(width: SCREEN_WIDTH, height: 140)
        }
        return GKBookCell.cellSize
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if listView {
            let cell = GKBookTableCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
           //
            return cell
        }
        let cell  = GKBookCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath);
       // cell.model = self.listData[indexPath.row]
        return cell;
    }
}
//
