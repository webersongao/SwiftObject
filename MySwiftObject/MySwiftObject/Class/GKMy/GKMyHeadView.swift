//
//  GKMyHeadView.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/9.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKMyHeadView: BaseView {

    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    lazy var tap: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapAction))
        return tap
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageV.layer.masksToBounds = true
        self.imageV.layer.cornerRadius = 35
        self.imageV.isUserInteractionEnabled = true
        self.imageV.addGestureRecognizer(self.tap)
        self.titleLab.textColor = UIColor.appx000000()
    }
    @objc func tapAction(){
        GKLogin.loginIn ? GKJump.jumpToUser() : GKLogin.needLogin(completion: { finish in
            
        })
    }
    var user :GKUser?{
        didSet{
            guard let model = user else { return  }
            self.imageV.setGkImageWithURL(url: model.avatar ?? "")
            self.titleLab.text = GKLogin.loginIn ? model.account! : "点击头像登录"
        }
    }
}
