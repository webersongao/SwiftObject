//
//  GKColorSelectController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/6/7.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
import Colorful
class GKColorSelectController: BaseViewController {

    lazy var colorPicker: ColorPicker = {
        let colorPicker = ColorPicker()
        colorPicker.addTarget(self, action: #selector(colorAction), for:.valueChanged)
        colorPicker.set(color: .white, colorSpace: .sRGB)
        return colorPicker
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "色盘")

        self.view.addSubview(self.colorPicker)
        self.colorPicker.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.width.equalTo(SCREEN_WIDTH)
        }
    }
    @objc func colorAction(){

        debugPrint(self.colorPicker.color.hex())
    }

}
