//
//  GKMyThemeCell.swift
//  BNovel
//
//  Created by wws1990 on 2021/8/12.
//

import UIKit

class GKMyThemeCell: BaseCollectionCell {

    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.backgroundColor = UIColor.appxffffff()
        self.button.isUserInteractionEnabled = false
        self.selectBtn.isUserInteractionEnabled = false
        self.mainView.layer.masksToBounds = true
        self.mainView.layer.cornerRadius = 6
        self.titleLab.textColor = UIColor.appx333333()
        
        let layerView = UIView()
        layerView.backgroundColor = UIColor.white
        self.contentView.insertSubview(layerView, at: 0)
        layerView.snp.makeConstraints { make in
            make.edges.equalTo(self.mainView)
        }
        layerView.layer.shadowColor = UIColor(white: 0, alpha: 0.1).cgColor
        layerView.layer.shadowOffset = CGSize(width: 0, height: 0  )
        layerView.layer.shadowOpacity = 1
        layerView.layer.shadowRadius = 6
        layerView.layer.cornerRadius = 6
        
        self.selectBtn.layer.masksToBounds = true
        self.selectBtn.layer.cornerRadius  =  15
        self.selectBtn.isHidden = true
    }
    var theme :GKTheme?{
        didSet{
            guard let theme = theme else { return }
            self.titleLab.text = theme.name
            self.button.backgroundColor = theme.defaultTheme ? UIColor.appx999999()  :UIColor(hex: theme.color)
        }
    }
    var selectCell : Bool?{
        didSet{
            guard let select = selectCell else { return }
            let str = select ? UIImage(named: "ic_mine_select") : UIImage.imageWithColor(color: UIColor.clear)
            self.button.setImage(str, for: .normal)
        }
    }
}
