//
//  GKStackViewController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/1/22.
//  Copyright © 2021 wangws1990. All rights reserved.
//
import UIKit

class GKStackViewController: BaseViewController {
    @IBOutlet weak var titleLab: UILabel!
    lazy var stackView : UIStackView = {
        return UIStackView()
    }()
    lazy var redView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.red
        return view
    }()
    lazy var yellowView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.yellow
        return view
    }()
    lazy var blueView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.blue
        return view
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title:"UIStackView")
        self.view.addSubview(self.stackView)
        self.stackView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(200)
        }
        self.stackView.addArrangedSubview(self.redView)
        self.stackView.addArrangedSubview(self.yellowView)
        self.stackView.addArrangedSubview(self.blueView)
        self.stackView.alignment = .fill
        self.stackView.distribution = .fillEqually
        self.stackView.axis = .horizontal
        self.stackView.spacing = 10
    }
}

