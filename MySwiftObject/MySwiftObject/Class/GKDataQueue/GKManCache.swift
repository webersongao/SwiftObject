//
//  GKManCache.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKManCache {
    public static func getChapters(commicId :String,
                                   success:@escaping ((_ object : GKManHistory?) ->()),
                                   failure:@escaping ((_ error : String) ->())){
        getNetChapter(comicId: commicId, sucesss: success, failure: failure)
    }
    public static func getCache(comicId :String,
                                chapterId:String,
                                sucesss:@escaping ((_ object : GKManContent) ->()),
                                failure:@escaping ((_ error : String) ->())){
        GKManCache.getContent(comicId: comicId, chapterId: chapterId) { model in
            if let item = model{
                sucesss(item)
            }else{
                getNetData(chapterId: chapterId, sucesss: { model in
                    sucesss(model)
                    if let _ = model.image_list{
                        insertCache(comicId:comicId, content: model) { finish in
                            
                        }
                    }
                }, failure: failure)
            }
        }
    }
    private static func insertCache(comicId :String, content :GKManContent,completion:@escaping ((_ success : Bool) -> ())){
        BaseQueue.insertData(tableName: comicId, primaryId: content.chapterId , content: content.toJSONString() ?? "", completion: completion)
    }
    private static func getContent(comicId:String, chapterId:String,completion:@escaping ((_ content : GKManContent?) -> ())){
        BaseQueue.searchData(tableName: comicId, primaryId: chapterId) { (json) in
            guard let data = [GKManContent].deserialize(from: json.rawString())else {
                return completion(nil)
            }
            if let model = data.first{
                completion(model)
            }else{
                completion(nil)
            }
        }
    }
    private static func getNetChapter(comicId :String,
                                      needload:Bool = false,
                                      sucesss :@escaping ((_ object :GKManHistory?) ->()),
                                      failure :@escaping ((_ error  :String) ->())){
        let blockData = { () ->() in
            ApiMoya.request(target: .mandetail(comicId), sucesss: { json in
                let histroy = GKManHistory()
                histroy.comicId = comicId

                let js = json["chapter_list"].rawString()
                let data = [GKManChapter].deserialize(from: js)
                if let list = data as? [GKManChapter]{
                    histroy.chapters = list
                }
                sucesss(histroy)
            }, failure: failure)
        }
        if needload{
            blockData()
        }else{
            let have = GKManHistoryData.haveData(comicId)
            if have{
                GKManHistoryData.getData(comicId: comicId, needChapter: true, completion: sucesss)
            }else{
                blockData()
            }
        }
    }
    //网络获取每一话的内容
    private static func getNetData(chapterId :String,
                                   sucesss:@escaping ((_ object : GKManContent) ->()),
                                   failure:@escaping ((_ error : String) ->())){
        ApiMoya.request(target: .manchapter(chapterId), sucesss: { json in
            guard let content = GKManContent.deserialize(from: json.rawString()) else { return failure("")}
            sucesss(content)

        }, failure: failure)
    }
}
