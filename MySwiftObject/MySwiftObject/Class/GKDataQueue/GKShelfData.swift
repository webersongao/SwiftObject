//
//  GKCaseDataQueue.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/9.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import SQLite.Swift

private let tableName = "bookCase";
//书架
class GKShelfData{
    static func haveData(_ bookId :String) ->Bool{
        return BaseShelfQueue().haveData(bookId)
    }
    static func insertBookModel(bookDetail :GKBook,sucesss:@escaping ((_ success : Bool) -> ())){
        BaseShelfQueue().insertData(bookId: bookDetail.bookId ?? "", bookInfo: bookDetail.toJSONString() ?? "") { success in
            if success{
                GKHomeNet.reloadHomeData(options: GKLoadOptions.base)
            }
            sucesss(success)
        }
    }
    //克林顿
    static func updateNew(bookId :String,newIcon :Int,lastChap :String? = nil,sucesss:@escaping ((_ success : Bool) ->())){
        BaseShelfQueue().updateNew(bookId: bookId, new: newIcon, lastChap: lastChap, completion: sucesss)
    }
    static func deleteBookModel(bookId:String,sucesss:@escaping ((_ success : Bool) ->())){

        BaseShelfQueue().deleteData(bookId: bookId) { success in
            if success{
                GKHomeNet.reloadHomeData(options: GKLoadOptions.base)
            }
            sucesss(success)
        }
    }
    static func getBookModel(bookId:String,sucesss:@escaping ((_ bookModel : GKBook? ) ->())){
        BaseShelfQueue().searchData(bookId: bookId, completion: sucesss)
        
    }
    static func getBookModels(page :Int,size: Int,sucesss:@escaping ((_ listData : [GKBook]) ->())){
        BaseShelfQueue().getData(page: page, size: size) { listData in
            sucesss(listData)
        }
    }
    static func getBookModels(sucesss:@escaping ((_ listData :[GKBook]) ->())){
        BaseShelfQueue().getData { datas in
            sucesss(GKShelfData.sortDatas(listDatas: datas, aspeing: false))
        }
    }
    ////yes 升序
    static func sortDatas(listDatas:[GKBook],aspeing : Bool) ->[GKBook]{
        var datas  = listDatas
//        datas.sort(by: {Double($0.updateTime) < Double($1.updateTime)})
        datas.sort { (model1, model2) -> Bool in
            return (Double(model1.topTime) < Double(model2.topTime))  ? aspeing : !aspeing;
        }
        return datas
    }
}
