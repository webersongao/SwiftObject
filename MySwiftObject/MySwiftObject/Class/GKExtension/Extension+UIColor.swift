//
//  Extension+UIColor.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/14.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
extension UIColor{
    static func appColor() ->UIColor{
        return UIColor(hex:"3991F0")
    }
    static func appx000000() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"9DA3AC")
            } else {
                return UIColor(hex:"000000")
            }
        }
        return color
       // return moya.night ? UIColor(hex: "9DA3AC") : UIColor(hex:"000000")
    }
    static func appx333333() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"636C78")
            } else {
                return UIColor(hex:"33383E")
            }
        }
        return color
       // return moya.night ? UIColor(hex:"636C78") : UIColor(hex:"33383E")
    }
    static func appx666666() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"636C78")
            } else {
                return UIColor(hex:"636C78")
            }
        }
        return color
//        return UIColor(hex:"636C78")
    }
    static func appx999999() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"636C78")
            } else {
                return UIColor(hex:"9DA3AC")
            }
        }
        return color
//        return moya.night ? UIColor(hex:"636C78") : UIColor(hex: "9DA3AC")
    }
    static func appxdddddd() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"33383E")
            } else {
                return UIColor(hex:"dddddd")
            }
        }
        return color
        //return moya.night ? UIColor(hex: "33383E"): UIColor(hex: "dddddd")
    }
    static func appxe5e5e5() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"33383E")
            } else {
                return UIColor(hex:"e5e5e5")
            }
        }
        return color
//        return moya.night ? UIColor(hex: "33383E") : UIColor(hex: "e5e5e5")
    }
    static func appxd0d0d0() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"33383E")
            } else {
                return UIColor(hex:"d0d0d0")
            }
        }
        return color
//        return moya.night ? UIColor(hex: "33383E") : UIColor(hex: "d0d0d0")
    }
    static func appxffffff() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            //debugPrint(trainCollection.userInterfaceStyle.rawValue)
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"101010")
            } else {
                return UIColor(hex:"ffffff")
            }
        }
        return color
        //return moya.night ? UIColor(hex:"000000") : UIColor(hex:"ffffff")
    }
    static func appxf1f2f4() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"33383E")
            } else {
                return UIColor(hex:"F1F2F4")
            }
        }
        return color
        //return moya.night ? UIColor(hex: "33383E") : UIColor(hex:"F1F2F4")
    }
    static func appxf8f8f8() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"181818")
            } else {
                return UIColor(hex:"f8f8f8")
            }
        }
        return color
//        return moya.night ? UIColor(hex: "101010") : UIColor(hex:"f8f8f8")
    }
    static func appxf4f4f4() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"333333")
            } else {
                return UIColor(hex:"f4f4f4")
            }
        }
        return color
        //return moya.night ? UIColor(hex: "101010") : UIColor(hex:"f4f4f4")
    }
    static func appAlertColor() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"181818")
            } else {
                return UIColor(hex:"ffffff")
            }
        }
        return color
//        return moya.night ? UIColor(hex: "181818") : UIColor.white
    }
}
