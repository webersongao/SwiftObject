//
//  UINavigationController+Color.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/11/3.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit
extension UINavigationController{
    public func setTabItem(normal :UIImage,selectedImage :UIImage){
        let color = UIColor(hex: moya.theme.tabColor)
        self.tabBarItem.image = normal.imageColor(UIColor.appx999999())
        self.tabBarItem.selectedImage = selectedImage.imageColor(color)
        self.tabBarItem.setTitleTextAttributes([.foregroundColor :UIColor.appx999999(),.font:UIFont.systemFont(ofSize: 13, weight: .regular)], for: .normal)
        self.tabBarItem.setTitleTextAttributes([.foregroundColor :color,.font:UIFont.systemFont(ofSize: 13, weight: .regular)], for: .selected)
        if let tabbar = self.tabBarController?.tabBar{
            tabbar.setTabbarColor()
        }
    }
    public func setNaviBackground(_ selectColor :UIColor = UIColor.white){
        let color = UIColor(hex: moya.theme.naviColor)
        let naviBar = self.navigationBar
        naviBar.setBackgroundImage(UIImage.imageWithColor(color: color), for: .default)
        naviBar.shadowImage = UIImage.imageWithColor(color: color)
        naviBar.titleTextAttributes = defaultNvi()
        naviBar.largeTitleTextAttributes = defaultLargeNvi()
        naviBar.isTranslucent = false
        ///MARK:切换皮肤会失败
        if #available(iOS 15.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundEffect = UIBlurEffect(style:.regular)
            appearance.shadowColor = color
            appearance.backgroundImage = UIImage.imageWithColor(color: color)
            appearance.backgroundColor = color
            appearance.titleTextAttributes = defaultNvi()
            appearance.largeTitleTextAttributes = defaultLargeNvi()
            naviBar.scrollEdgeAppearance = appearance
            naviBar.standardAppearance = appearance
            naviBar.compactAppearance = appearance
            naviBar.compactScrollEdgeAppearance = appearance
        } else {
            // Fallback on earlier versions
        }
        naviBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .automatic
    }
    private func defaultNvi()-> [NSAttributedString.Key : Any]{
        let color = UIColor(hex: moya.theme.naviTitleColor)
        var dic :[NSAttributedString.Key : Any] = [ : ]
        dic.updateValue(UIFont.systemFont(ofSize: 18, weight: UIFont.Weight(rawValue: 500)), forKey: .font)
        dic.updateValue(color, forKey: .foregroundColor)
        return dic
    }
    private func defaultLargeNvi()-> [NSAttributedString.Key : Any]{
        let color = UIColor(hex: moya.theme.naviTitleColor)
        var dic :[NSAttributedString.Key : Any] = [ : ]
        dic.updateValue(UIFont.systemFont(ofSize: 30, weight: UIFont.Weight(rawValue: 700)), forKey: .font)
        dic.updateValue(color, forKey: .foregroundColor)
        return dic
    }
}


extension UITabBar{
    func setTabbarColor(){
        let color = UIColor(hex: moya.theme.tabColor)
        let tabColor = UIColor.appxffffff()
        self.tintColor = color
        self.unselectedItemTintColor = UIColor.appx999999()
        self.shadowImage = UIImage.imageWithColor(color:tabColor)
        self.backgroundImage = UIImage.imageWithColor(color: tabColor)
        self.backgroundColor = tabColor
    
        let lineColor = moya.night ? UIColor(hex: "333333") : UIColor(hex: "e5e5e5")
        let appearance = UITabBarAppearance()
        appearance.backgroundColor = tabColor
        appearance.shadowColor = tabColor
        appearance.shadowImage = UIImage.imageWithColor(color:lineColor)
        appearance.backgroundEffect = UIBlurEffect(style: .regular)
        self.standardAppearance = appearance
        if #available(iOS 15.0, *) {
            self.scrollEdgeAppearance = appearance
        } else {
            // Fallback on earlier versions
        }
    }
}
