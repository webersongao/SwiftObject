//
//  GKNumber.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/23.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

extension Int{
    var getCount :String{
        if self > 10000 {
            let value :Float = Float(self/10000)
            return String(value)+"万"
        }
        return String(self);
    }
}
