//
//  Date+Time.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/11/4.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit
extension Date{
    //MARK:yyyy/MM/dd 是否是同一天
    public static func sameDay(_ timeStamp :TimeInterval) ->Bool{
        let date = Date()
        let today = date.timeStampToDay(timeStamp: date.timeStamp())
        let current = date.timeStampToDay(timeStamp: timeStamp)
        return today == current
    }
    //MARK:是否超过指定的时间 5分钟
    public static func outTime(_ timeStamp :TimeInterval)->Bool{
        let date = Date()
        let current = date.timeStamp()
        let time = current - timeStamp
        return time > 5 * 60
    }
    public func timeStampToDay(timeStamp :TimeInterval) -> String{
        let date = Date(timeIntervalSince1970: timeStamp)
        let formatter:DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter.string(from: date)
    }
    //MARK:yyyy/MM/dd HH
    public func timeStampToHouse(timeStamp :TimeInterval) -> String{
        let date = Date(timeIntervalSince1970: timeStamp)
        let formatter:DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH"
        return formatter.string(from: date)
    }
    //MARK:yyyy/MM/dd HH:mm
    public func timeStampToMinute(timeStamp :TimeInterval) -> String{
        let date = Date(timeIntervalSince1970: timeStamp)
        let formatter:DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        return formatter.string(from: date)
    }
    //MARK:yyyy/MM/dd HH:mm:ss
    public func timeStampToSecond(timeStamp :TimeInterval) -> String{
        let date = Date(timeIntervalSince1970: timeStamp)
        let formatter:DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        return formatter.string(from: date)
    }
    //MARK:HH:mm
    public func timeStampToDateMinute() -> String{
        let date = Date(timeIntervalSinceNow: 0)
        let formatter:DateFormatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: date)
    }
    //MARK:HH:mm:ss
    public func timeStampToDateSecond(timeStamp :TimeInterval) -> String{
        let date = Date(timeIntervalSince1970: timeStamp)
        let formatter:DateFormatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter.string(from: date)
    }
    public func timeStamp() -> TimeInterval{
        return self.timeIntervalSince1970
    }

}
