//
//  GKBrowseModel.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import SwiftyJSON

class GKHistory: HandyJSON {
    var bookId     :String?       = ""
    var insertTime :TimeInterval  = 0
    var chapter    :NSInteger     = 0
    var pageIndex  :NSInteger     = 0
    
    var book       :GKBook?        = nil
    var source     :GKSource?      = nil
    var chapters   :[JSON]?        = nil//章节列表
    required init() {}
}
