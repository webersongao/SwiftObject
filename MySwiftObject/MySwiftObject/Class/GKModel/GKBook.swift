//
//  GKBook.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import RealmSwift

class GKBook:HandyJSON {
    var bookId    :String?       = "";
    var updated   :String        = ""
    var author    :String        = "";
    var cover     :String? = "";
    var shortIntro:String? = ""
    var title     :String? = "";
    var majorCate :String? = "";
    var minorCate :String? = "";
    var lastChapter:String? = "";
   
    var retentionRatio :Float  = 0.0;
    var latelyFollower :Int    = 0;
   
    var new            :Bool   = false
    var isSerial       :Bool   = false
    var updateTime:TimeInterval  = 0//更新时间用于排序
    var insertTime:TimeInterval  = 0//更新时间用于排序
    var topTime   :TimeInterval  = 0
    func mapping(mapper: HelpingMapper) {
        mapper <<< self.bookId     <-- ["bookId","_id"]
        mapper <<< self.shortIntro <-- ["shortIntro","longIntro"]
    }
    required init() {}
//    override class func primaryKey() -> String? {
//        return "bookId"
//    }
}

class GKBookInfo: GKBook {
    var cat          :String? = ""
    var chaptersCount:String? = ""
    var creater      :String? = ""
    var postCount    :Int  = 0
    var wordCount    :Int  = 0
    
    var hasSticky    :Bool? = false
    var hasUpdated   :Bool? = false
    var hasCp        :Bool? = false
    var donate       :Bool? = false
    var le           :Bool? = false
    var allowVoucher :Bool? = false
    
    var numberLine   :Int = 3
}
class GKBookUpdateInfo : GKBookInfo{
    var tags         :[String] = []
    var copyrightInfo:String   = ""
}
