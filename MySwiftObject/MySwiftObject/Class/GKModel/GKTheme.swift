//
//  GKTheme.swift
//  BNovel
//
//  Created by wws2021 on 2021/9/3.
//

import UIKit
//MARK:主题皮肤
class GKTheme :HandyJSON {
    var name           :String   = BThemeState.defaults.rawValue//MARK:主题名字
    var color          :String   = "3991F0"//主题颜色
    var naviColor      :String   = "FFFFFF"//导航栏背景色
    var naviTitleColor :String   = "000000"//导航栏标题颜色
    var naviRightColor :String   = "333333"//导航栏右侧文字颜色
    var tabColor       :String   = "3991F0"//tab原色
    
    var naviBack       :String   = "icon_nav_back"//导航栏返回按钮icon
    
    var fileName       :String?  = nil//文件名字
    var defaultTheme   :Bool     = true//是否是默认主题
    required public init() {}
}
