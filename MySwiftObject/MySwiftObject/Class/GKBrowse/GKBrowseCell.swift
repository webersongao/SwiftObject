//
//  GKBrowseCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/2/22.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKBrowseCell: BaseTableCell {

    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var openBtn: UIButton!
    @IBOutlet weak var contentLab: UILabel!
    @IBOutlet weak var subTitleLab: UILabel!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLab.textColor = UIColor.appx333333()
        self.subTitleLab.textColor = UIColor.appx666666()
        self.contentLab.textColor = UIColor.appx999999()
        self.openBtn.layer.masksToBounds = true
        self.openBtn.layer.cornerRadius = 15
        self.openBtn.layer.borderWidth = 0.5
        self.openBtn.layer.borderColor = UIColor.appxe5e5e5().cgColor
        self.openBtn.setTitleColor(UIColor.appx333333(), for: .normal)
        self.lineView.backgroundColor = UIColor.appxe5e5e5()
        self.imageV.layer.masksToBounds = true
        self.imageV.layer.cornerRadius = AppRadius
    }
    var model : GKBook?{
        didSet{
            guard let item = model else { return }
            self.imageV.setGkImageWithURL(url: item.cover  ?? "")
            self.titleLab.text = item.title ?? ""
            self.contentLab.attributedText  = attTitle(content: item.shortIntro ?? "")
            self.subTitleLab.text = "\(item.author)/\(item.majorCate ?? "")"
        }
    }
    @IBAction func openAction(_ sender: UIButton) {
        guard let bookModel = self.model else { return }
        GKJump.jumpToNovel(bookId: bookModel.bookId)
    }
    func attTitle(content :String) -> NSAttributedString {
        let paragraphStyle  = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = .left
        paragraphStyle.allowsDefaultTighteningForTruncation = true
         let att = NSAttributedString(string:content.count > 0 ? content : "这家伙很懒,暂无简介!", attributes:[NSAttributedString.Key.paragraphStyle : paragraphStyle,NSAttributedString.Key.foregroundColor : UIColor.appx999999()])
        return att
    }
}

