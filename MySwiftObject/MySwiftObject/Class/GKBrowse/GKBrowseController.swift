//
//  GKBrowseController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKBrowseController: BaseTableViewController {
    private lazy var listData : [GKHistory] = {
        return []
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title:"浏览历史")
        self.setupRefresh(scrollView: self.tableView, options: .defaults)
    }
    override func refreshData(page: Int) {
        GKHistoryData.getHistoryData { listData in
            if page == RefreshPageStart{
                self.listData.removeAll()
            }
            let data = listData.filter { history in
                return history.book != nil
            }
            self.listData = self.sortDatas(listDatas: data,aspeing: false)
            self.tableView.reloadData()
            self.endRefresh(more: false)
        }
    }
    //MARK: true
    private func sortDatas(listDatas:[GKHistory],aspeing : Bool = true) ->[GKHistory]{
        var datas  = listDatas
        datas.sort { (model1, model2) -> Bool in
            return Double(model1.insertTime) < Double(model2.insertTime)  ? aspeing : !aspeing;
        }
        return datas
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = GKBrowseCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        cell.model = self.listData[indexPath.row].book
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView.style == .plain {
            return
        }
        let model = self.listData[indexPath.row]
        guard let bookModel = model.book else { return  }
        GKJump.jumpToDetail(bookId: bookModel.bookId)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
         let context = UIContextualAction(style: .destructive, title: "删除") { row, index, finish in
             self.deleteAction(indexPath: indexPath)
         }
        let sw = UISwipeActionsConfiguration(actions: [context])
        sw.performsFirstActionWithFullSwipe = false
        return sw
    }
    func deleteAction(indexPath:IndexPath){
        let model = self.listData[indexPath.row]
        GKHistoryData.updateBook(bookId: model.bookId ?? "", bookInfo: "") { success in
            if self.listData.count > indexPath.row{
                self.listData.remove(at: indexPath.row)
                self.tableView.reloadData();
            }
        }
    }
}
