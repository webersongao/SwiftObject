//
//  ApiMoya.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/10.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
import Moya
public enum ApiMoya{
    case homeHot(_ hotId :String)
    case homeSex
    case search(_ keyword :String,_ page :Int)
    case commend(_ bookId :String)
    case classify
    case classifyTail(_ group:String,name :String,page :Int)
    case bookDetail(_ bookId :String)
    case bookUpdate(_ bookId :String)
    
    case source(_ bookId :String)
    case chapters(_ sourceId :String)
    case content(_ link :String)
    
    case mandetail(_ comicId :String)
    case manchapter(_ chapterId :String)
}
extension ApiMoya :TargetType{
    public var path: String {
        var txCode :String = ""
        switch self {
        case .homeSex:
            txCode = "ranking/gender"
        case let .homeHot(hotId):
            txCode = "ranking/\(hotId)"
        case .search:
            txCode = "book/fuzzy-search"
        case let .commend(bookId):
            txCode = "book/" + bookId + "/recommend"
        case .classify:
            txCode = "cats/lv2/statistics"
        case .classifyTail:
            txCode = "book/by-categories"
        case let .bookDetail(bookId):
            txCode = "book/\(bookId)"
        case let .bookUpdate(bookId):
            txCode = "book/\(bookId)"
        
        case .source:
            txCode = "toc"
        case let .chapters(sourceId):
            txCode = "toc/\(sourceId)"
        case let .content(link):
            txCode = GKTool.stringURLEncode(link)
        
        case .mandetail:
            txCode = "comic/detail_static_new"
        case .manchapter:
            txCode = "comic/chapterNew"
        }
        return txCode
    }
    public var method: Moya.Method {
        return .get
    }
    public var baseURL: URL {
        var url = ""
        switch self {
        case .content:
            url = "https://chapter2.zhuishushenqi.com/chapter"
            break
        case .manchapter,.mandetail:
            url = "http://app.u17.com/v3/appV3_3/ios/phone"
            break
        default:
            url = "https://api.zhuishushenqi.com"
            break
        }
        guard let url = URL(string: url) else { return URL(fileURLWithPath: "") }
        return url
    }
    public var headers: [String : String]? {
        var header : [String: String] = [:]
        header.updateValue("Content-Type", forKey: "Content-Type")
        header.updateValue("chrome", forKey: "User-Agent")
        return header
    }
}
