//
//  ApiMoyaBase.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/10.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

public  let moya             = ApiMoyaBase.manager

public class ApiMoyaBase{
    let system      :String = "iOS"
    let timeOut     :TimeInterval = 10//超时

    let uuid        :String = uuid()
    let build       :String = build()
    let version     :String = version()
    let bundleId    :String = bundleId()
    let appName     :String = appName()
    var voiceNumber :Float  = 0
    ///MARK:客户端配置信息
    lazy var client :ApiClient = ApiClient.client
    lazy var bookSet :GKBookSet = GKBookSet.bookSet
    lazy var user :GKUser = GKLogin.user
    //GKUser
    private lazy var nightTheme: GKTheme = {
        return ApiClient.nightTheme
    }()
    var theme :GKTheme{
        let the = moya.client.theme ?? ApiClient.defaultTheme
        return moya.night ? moya.nightTheme : the
    }
    var night :Bool{
        return moya.client.night
    }
    var landscape :Bool{
        return UIScreen.main.bounds.size.width > UIScreen.main.bounds.size.height
    }
    var safe_top : CGFloat{
        let top  = BaseMacro.iPhone_Bar().statusBar
        return top
    }
    var safe_bottom :CGFloat{
        let bottom  = BaseMacro.iPhone_Bar().tabBar
        return bottom
    }
    var safe_width :CGFloat{
        return UIScreen.main.bounds.size.width
    }
    var safe_height :CGFloat{
        return UIScreen.main.bounds.size.height
    }
    var listView :Bool{
        return moya.client.gird == .list
    }
    var listTop :CGFloat{
        return listView ? 0 : 5
    }
    ///MARK:app的主颜色 换皮肤后这个颜色会跟着改变
    var appColor :UIColor{
        return UIColor(hex: moya.theme.color)
    }
    var bookChapterFont :UIFont{
        return UIFont.systemFont(ofSize: CGFloat(moya.bookSet.fontSize + 4), weight: .semibold)
    }
    var separated :String{
        return "\n\r"
    }
    ///MARK:UserDefaults存储的key统一写在这里
    let defaultLogin   = "defaultLogin"//用户信息
    let defaultClient  = "defaultClient"//客户端信息
    let defaultBookSet = "defaultBookSet"//读书设置
    ///MARK:App的一些信息
    fileprivate static func bundleId() ->String{
        return Bundle.main.bundleIdentifier ?? ""
    }
    fileprivate static func uuid() ->String{
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
    fileprivate static func build() ->String{
        let json = JSON(Bundle.main.infoDictionary as Any)
        return json["CFBundleVersion"].stringValue
    }
    fileprivate static func version() ->String{
        let json = JSON(Bundle.main.infoDictionary as Any)
        return json["CFBundleShortVersionString"].stringValue
    }
    fileprivate static func appName() ->String{
        let json = JSON(Bundle.main.infoDictionary as Any)
        return json["CFBundleDisplayName"].stringValue
    }
    fileprivate static let manager = ApiMoyaBase()
}

