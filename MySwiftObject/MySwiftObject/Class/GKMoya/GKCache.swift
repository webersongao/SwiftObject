//
//  GKCache.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/6/23.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit
import YYCache

private let bookCache = YYCache(name:"bookCache")
class ApiCache {
    public static func removeAll(){
        guard let cache = bookCache else { return  }
        cache.removeAllObjects()
        debugPrint("ApiCache class")
    }
    public static func clear(key :String){
        guard let cache = bookCache else { return  }
        cache.removeObject(forKey: key)
    }
    public static func set(object : [String:Any],key :String){
        guard let cache = bookCache else { return  }
        cache.setObject(object as NSCoding, forKey: key)
    }
    public static func values(key: String) -> (json :JSON,empty :Bool){
        guard let cache = bookCache else { return (JSON(),true)}
        let object = cache.object(forKey: key)
        let json = JSON(object as Any)
        return (json,json.isEmpty)
    }
}
extension ApiCache{
    public static func removeAll(){
        guard let cache = bookCache else { return  }
        cache.removeAllObjects()
        debugPrint("ApiCache extension")
    }
}
