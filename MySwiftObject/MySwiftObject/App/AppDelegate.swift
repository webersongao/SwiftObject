//
//  AppDelegate.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/12/3.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import Lottie

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var rotation : UIInterfaceOrientationMask = .portrait
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        BaseRouter.startRouter()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        self.window?.backgroundColor = UIColor.white
        let vc = GKLaunchController {
            self.window?.rootViewController = GKTabBarController()
            self.window?.overrideUserInterfaceStyle = moya.night ? .dark : .light
        }
        self.window?.rootViewController = vc
        return true
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return  self.rotation == .portrait ? .portrait : .landscapeRight
    }
    func applicationWillResignActive(_ application: UIApplication) {
        debugPrint("applicationWillResignActive")
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        debugPrint("applicationWillEnterForeground")
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        debugPrint("applicationDidBecomeActive")
    }
    
//    func applicationDidEnterBackground(_ application: UIApplication) {
//        debugPrint("applicationDidEnterBackground")
//        var identy : UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.init(rawValue: 0)
//        identy = UIApplication.shared.beginBackgroundTask {
//            debugPrint("endBackgroundTask")
//            application.endBackgroundTask(identy)
//        }
//    }
//    func applicationWillTerminate(_ application: UIApplication) {
//        debugPrint("applicationWillTerminate")
//        let path  = URL(string: "https://api.zhuishushenqi.com/cats/lv2/statistics")!
//        let conifg = URLSessionConfiguration.default;
//        let session  = URLSession(configuration: conifg);
//        var request = URLRequest(url: path)
//        request.timeoutInterval = 10;
//        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type")
//        request.addValue("chrome", forHTTPHeaderField:"User-Agent")
//        let task  = session.dataTask(with: request) { (data, respond, error) in
//            let json = JSON(data as Any)
//            debugPrint(json)
//        }
//        task.resume();
//        sleep(2)
//    }
}
